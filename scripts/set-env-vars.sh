#!/bin/bash
export RECEIVER_STORE_INTERVAL="3s"

export RECEIVER_NATS_HOST = "nats_service_nats_server"
export RECEIVER_NATS_PORT = "4222"