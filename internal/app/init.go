package app

import (
	"context"
	"fmt"
	"github.com/nats-io/nats.go"
	"gitlab.com/vkenrik117/uni-study-receiver/internal/configs"
	worker "gitlab.com/vkenrik117/uni-study-receiver/internal/nats"
	"gitlab.com/vkenrik117/uni-study-receiver/internal/store/memory"
	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"
)

type App struct {
	nats *worker.NATS
}

func Initialize(ctx context.Context) (*App, error) {
	natsConfig, err := configs.NewConfigNATS()
	if err != nil {
		return nil, err
	}
	memoryStoreConfig, err := configs.NewMemoryStoreConfig()
	if err != nil {
		return nil, err
	}

	logger, err := zap.NewDevelopment()
	if err != nil {
		return nil, fmt.Errorf("could not create new logger: %s", err.Error())
	}

	memoryStore := memory.New(memoryStoreConfig.ClearInterval)
	memoryStore.StartAutoClearing(ctx)

	natsConnectionString := fmt.Sprintf("nats://%s:%s",
		natsConfig.Host,
		natsConfig.Port)
	connection, err := nats.Connect(natsConnectionString)
	if err != nil {
		return nil, fmt.Errorf("connect: %s", err.Error())
	}

	newWorker, err := worker.New(connection, logger)
	if err != nil {
		return nil, fmt.Errorf("could not create new worker: %s", err.Error())
	}

	dataDrainer := worker.NewDataDrainer(memoryStore, logger)
	err = newWorker.AddWorker("STUDY.*", dataDrainer)
	if err != nil {
		return nil, err
	}

	return &App{
		nats: newWorker,
	}, nil
}

func (a *App) Run(ctx context.Context) error {
	errGroup, ctx := errgroup.WithContext(ctx)

	errGroup.Go(func() error {
		return a.nats.Start(ctx)
	})
	errGroup.Go(func() error {
		<-ctx.Done()
		a.nats.Stop()
		return nil
	})

	return errGroup.Wait()
}
