package nats

import (
	"context"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/vkenrik117/uni-study-receiver/internal/store/memory"
	"go.uber.org/zap"
)

// DataDrainer забирает данные полученные из NATS и отправляет их в хранилище,
// где эти данные через некоторое время удаляются
type DataDrainer struct {
	store  *memory.Store
	logger *zap.Logger
}

func NewDataDrainer(memoryStore *memory.Store, logger *zap.Logger) *DataDrainer {
	return &DataDrainer{
		store:  memoryStore,
		logger: logger,
	}
}

func (d *DataDrainer) Handle(ctx context.Context, message []byte) error {
	d.store.Set(uuid.NewV4().String(), message)

	return nil
}
