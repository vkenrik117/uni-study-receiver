package configs

import (
	"fmt"
	"github.com/kelseyhightower/envconfig"
	"time"
)

// MemoryStoreConfig конфигурация временного хранилища в оперативной памяти
type MemoryStoreConfig struct {
	// ClearInterval это период времени между очистками хранилища
	ClearInterval time.Duration `envconfig:"RECEIVER_STORE_INTERVAL"`
}

func NewMemoryStoreConfig() (*MemoryStoreConfig, error) {
	var memoryStoreConfig MemoryStoreConfig
	err := envconfig.Process("receiver_store", &memoryStoreConfig)
	if err != nil {
		return nil, fmt.Errorf("could not process memory store env: %s", err.Error())
	}
	return &memoryStoreConfig, nil
}
