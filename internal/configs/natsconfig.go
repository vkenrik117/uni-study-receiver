package configs

import (
	"fmt"
	"github.com/kelseyhightower/envconfig"
)

type ConfigNATS struct {
	Host string `envconfig:"RECEIVER_NATS_HOST"`
	Port string `envconfig:"RECEIVER_NATS_PORT"`
}

func NewConfigNATS() (*ConfigNATS, error) {
	var natsConfig ConfigNATS
	err := envconfig.Process("receiver_app", &natsConfig)
	if err != nil {
		return nil, fmt.Errorf("could not process nats server env: %s", err.Error())
	}
	return &natsConfig, nil
}
