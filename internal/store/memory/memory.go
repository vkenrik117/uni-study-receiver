package memory

import (
	"context"
	"sync"
	"time"
)

// Store простое хранилище, с функцией самоотчистки
type Store struct {
	sync.RWMutex
	items    map[string][]byte
	interval time.Duration
}

func New(interval time.Duration) *Store {
	items := make(map[string][]byte)

	store := Store{
		items:    items,
		interval: interval,
	}

	return &store
}

func (s *Store) Set(key string, value []byte) {
	s.Lock()

	defer s.Unlock()

	s.items[key] = value
}

func (s *Store) Get(key string) ([]byte, bool) {

	s.RLock()

	defer s.RUnlock()

	item, found := s.items[key]

	if !found {
		return nil, false
	}

	return item, true
}

func (s *Store) Clear() {
	s.items = make(map[string][]byte)
}

func (s *Store) StartAutoClearing(ctx context.Context) {
	ticker := time.NewTicker(s.interval)
	go func() {
	outerLoop:
		for {
			select {
			case <-ticker.C:
				s.Clear()
			case <-ctx.Done():
				break outerLoop
			}
		}
	}()
}
