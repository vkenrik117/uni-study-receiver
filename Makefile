# NATS
.PHONY: "create-example-nats-stream"
create-example-nats-stream:
	nats stream add STUDY --subjects "STUDY.*" --ack --max-msgs=-1 --max-bytes=-1 --max-age=1y --storage file --retention limits --max-msg-size=-1 --discard=old

# Docker image
.PHONY: "build"
build:
	CGO_ENABLED=0 GOOS=linux go build -a -o main ./cmd/uni-study-receiver/

.PHONY: "create-docker-image"
create-docker-image:
	docker build -t uni-study-receiver-scratch -f Dockerfile.scratch .

# Docker network
.PHONY: "create-docker-network"
create-docker-network:
	docker network create uni_study_receiver

.PHONY: "docker-connect-nats"
docker-connect-nats:
	docker network connect uni_study_receiver nats_service_nats_server

# Docker example
.PHONY: "run-first-example-container"
run-first-example-container:
	docker run --network nats_service -d -p 3000:3000 \
	--env RECEIVER_STORE_INTERVAL="3s" \
	--env RECEIVER_NATS_HOST = "nats_service_nats_server" \
	--env RECEIVER_NATS_PORT = "4222" \
	 slvic/uni-study-receiver-scratch:latest

.PHONY: "run-second-example-container"
run-second-example-container:
	docker run --network nats_service -d -p 3001:3000 \
	--env RECEIVER_STORE_INTERVAL="3s" \
	--env RECEIVER_NATS_HOST = "nats_service_nats_server" \
	--env RECEIVER_NATS_PORT = "4222" \
    slvic/uni-study-receiver-scratch:latest

.PHONY: "run-example"
run-example: run-first-example-container run-second-example-container